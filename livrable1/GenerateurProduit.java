class GenerateurProduit extends Program { 
    String rechercherValeur(String file, String cle) {
        String resultat ="";
        for(int i = 0; i<length(file);i=i+1){
            if (charAt(file,i) == charAt(cle,0) && resultat==""){
                println("1er IF");
                println(i);
                if (equals(substring(file,i,length(cle)+2),cle+" :")){
                    println("2er IF");
                    println(substring(file,i,length(cle)+2));
                    int cpt=length(cle)+1;
                        while(charAt(file,cpt)!='\n'){
                            resultat=resultat+ charAt(file,cpt);
                            cpt=cpt+1;
                            println(resultat);
                        }
                }
            }
        }
        println(resultat);
        return(resultat);
    }
    void algorithm(){
        String nom = rechercherValeur(fileAsString("./data/produit1.txt"), "nom");
        println(nom);
        String date = rechercherValeur(fileAsString("./data/produit1.txt"), "date");
        println(date);
        String entreprise = rechercherValeur(fileAsString("./data/produit1.txt"), "entreprise");
        println(entreprise);
        String prix = rechercherValeur(fileAsString("./data/produit1.txt"), "prix");
        println(prix);
        String description = rechercherValeur(fileAsString("./data/produit1.txt"), "description");
        println(description);
    }
}