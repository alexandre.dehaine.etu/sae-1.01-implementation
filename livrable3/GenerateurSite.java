class GenerateurSite extends Program { 

    final char NEW_LINE = '\n';
    final int IDX_NOM =0;
    final int IDX_DATE=1;
    final int IDX_ENTREPRISE=2;
    final int IDX_PRIX=3;
    final int IDX_DESCRIPTION=4;

    String rechercherValeur(String file, String cle) {

        String resultat ="";
        for(int i = 0; i<length(file) && resultat=="" ;i=i+1){
            if (charAt(file,i) == charAt(cle,0) ){
                if (equals(substring(file,i,i+length(cle)+2),cle+" :")){
                    int cpt=length(cle)+3;
                        while(charAt(file,i+cpt)!='\n'){
                            resultat=resultat+ charAt(file,i+cpt);
                            cpt=cpt+1;
                        }
                }
            }
        }
        return(resultat);
    }

    String genererSite(String nomFichier, int index){
        String content = "";
        String[][] tab = chargerProduits("./data/","produit");
        if (equals(nomFichier,"index")){
            content = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">"+ NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>"+ NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>"+ NEW_LINE +
            "        <li><a href=\"produit1.html\">"+tab[1][IDX_NOM]+"</a></li>"+ NEW_LINE +
            "        <li><a href=\"produit2.html\">"+tab[2][IDX_NOM]+"</a></li>"+ NEW_LINE +
            "        <li><a href=\"produit3.html\">"+tab[3][IDX_NOM]+"</a></li>"+ NEW_LINE +
            "        <li><a href=\"produit4.html\">"+tab[4][IDX_NOM]+"</a></li>"+ NEW_LINE +
            "        <li><a href=\"produit5.html\">"+tab[5][IDX_NOM]+"</a></li>"+ NEW_LINE +
            "      </ul>"+ NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>" + NEW_LINE +
            "          <p>" + NEW_LINE +
            "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique." + NEW_LINE + 
            "          </p>" + NEW_LINE + 
            "      </section>" + NEW_LINE + 
            "    </main>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
        }else{
            content = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">"+ NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>"+ NEW_LINE +
            "      <ul>"+ NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>"+ NEW_LINE;
            if(index<length(getAllFilesFromDirectory("./data/"))-5){
                println("<26");
                content = content + "        <li><a href=\""+nomFichier+(index+1)+".html\">"+tab[index][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                content = content + "        <li><a href=\""+nomFichier+(index+2)+".html\">"+tab[index+1][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                content = content + "        <li><a href=\""+nomFichier+(index+3)+".html\">"+tab[index+2][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                content = content + "        <li><a href=\""+nomFichier+(index+4)+".html\">"+tab[index+3][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                content = content + "        <li><a href=\""+nomFichier+(index+5)+".html\">"+tab[index+4][IDX_NOM]+"</a></li>"+ NEW_LINE ;
            }else if(index==length(getAllFilesFromDirectory("./data/"))-5){
                for(int i=length(getAllFilesFromDirectory("./data/"))-5;i<=length(getAllFilesFromDirectory("./data/"))-1;i++){
                    println("26");
                    println("i ="+i);
                    content = content + "        <li><a href=\""+nomFichier+(i+1)+".html\">"+tab[i][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                }
            }else if(index==length(getAllFilesFromDirectory("./data/"))-4){
                for(int i=length(getAllFilesFromDirectory("./data/"))-4;i<=length(getAllFilesFromDirectory("./data/"))-1;i++){
                    println("27");
                    println("i ="+i);
                    content = content + "        <li><a href=\""+nomFichier+(i+1)+".html\">"+tab[i][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                }
            }else if(index==length(getAllFilesFromDirectory("./data/"))-3){
                for(int i=length(getAllFilesFromDirectory("./data/"))-3;i<=length(getAllFilesFromDirectory("./data/"))-1;i++){
                    println("28");
                    println("i ="+i);
                    content = content + "        <li><a href=\""+nomFichier+(i+1)+".html\">"+tab[i][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                }
            }else if(index==length(getAllFilesFromDirectory("./data/"))-2){
                for(int i=length(getAllFilesFromDirectory("./data/"))-2;i<=length(getAllFilesFromDirectory("./data/"))-1;i++){
                    println("29");
                    println("i ="+i);
                    content = content + "        <li><a href=\""+nomFichier+(i+1)+".html\">"+tab[i][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                }
            }else if(index==length(getAllFilesFromDirectory("./data/"))-1){
                for(int i=length(getAllFilesFromDirectory("./data/"))-1;i<=length(getAllFilesFromDirectory("./data/"))-1;i++){
                    println("+29");
                    println("i ="+i);
                    content = content + "        <li><a href=\""+nomFichier+(i+1)+".html\">"+tab[i][IDX_NOM]+"</a></li>"+ NEW_LINE ;
                }
            }
            content = content + "      </ul>"+ NEW_LINE +
            "    </nav>"+ NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>" + tab[index][IDX_NOM] + " (" + tab[index][IDX_ENTREPRISE] + ")</h2>" + NEW_LINE +
            "        <h3>" + tab[index][IDX_PRIX] + " (Sortie en " + tab[index][IDX_DATE] + ")</h3>" + NEW_LINE +
            "        <p>" + NEW_LINE +
            tab[index][IDX_DESCRIPTION] + NEW_LINE + 
            "        </p>" + NEW_LINE + 
            "      </section>" + NEW_LINE + 
            "    </main>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
        }
        return(content);
    }

    String[][] chargerProduits(String repertoire, String prefixe) {
        String[][] resultat = new String[length(getAllFilesFromDirectory("./data/"))][5];
        for (int i = 0; i<length(resultat,1);i++){
            resultat[i][IDX_NOM]=rechercherValeur(fileAsString("./data/"+prefixe+(i+1)+".txt"), "nom");
            resultat[i][IDX_DATE]=rechercherValeur(fileAsString("./data/"+prefixe+(i+1)+".txt"), "date");
            resultat[i][IDX_ENTREPRISE]=rechercherValeur(fileAsString("./data/"+prefixe+(i+1)+".txt"), "entreprise");
            resultat[i][IDX_PRIX]=rechercherValeur(fileAsString("./data/"+prefixe+(i+1)+".txt"), "prix");
            resultat[i][IDX_DESCRIPTION]=rechercherValeur(fileAsString("./data/"+prefixe+(i+1)+".txt"), "description");
        }

        //Pour imprimer le tableau (illisible mais pratique :D)
        /*for (int i=0; i<length(resultat,1);i++){
            for(int j=0; j<length(resultat,2);j++){
                print(resultat[i][j]+" | ");
            }
            print("\n");
        }*/

        return(resultat);
    }

    void algorithm(){
        String[][] tab = chargerProduits("./data/","produit");
        for(int i=-1;i<length(tab,1);i++){
            if(i==-1){
                println(i);
                stringAsFile("output/index.html", genererSite("index",-1));
            }else{
                println(i);
                stringAsFile("output/produit"+(i+1)+".html", genererSite("produit",i));
            }
        }
    }
}
