class GenerateurSite extends Program { 

    final char NEW_LINE = '\n';

    String rechercherValeur(String file, String cle) {

        String resultat ="";
        for(int i = 0; i<length(file) && resultat=="" ;i=i+1){
            if (charAt(file,i) == charAt(cle,0) ){
                if (equals(substring(file,i,i+length(cle)+2),cle+" :")){
                    int cpt=length(cle)+3;
                        while(charAt(file,i+cpt)!='\n'){
                            resultat=resultat+ charAt(file,i+cpt);
                            cpt=cpt+1;
                        }
                }
            }
        }
        return(resultat);
    }

    String genererSite(String nomFichier){
        String content = "";
        if (equals(nomFichier,"index")){
            content = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">"+ NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Produit 1</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">Produit 2</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Produit 3</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Produit 4</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">Produit 5</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !</h2>" + NEW_LINE +
            "          <p>" + NEW_LINE +
            "Bienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique." + NEW_LINE + 
            "          </p>" + NEW_LINE + 
            "      </section>" + NEW_LINE + 
            "    </main>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
        }else{
            String nom = rechercherValeur(fileAsString("./data/"+nomFichier+".txt"), "nom");
            String date = rechercherValeur(fileAsString("./data/"+nomFichier+".txt"), "date");
            String entreprise = rechercherValeur(fileAsString("./data/"+nomFichier+".txt"), "entreprise");
            String prix = rechercherValeur(fileAsString("./data/"+nomFichier+".txt"), "prix");
            String description = rechercherValeur(fileAsString("./data/"+nomFichier+".txt"), "description");

            content = 
            "<!DOCTYPE html>" + NEW_LINE +
            "<html lang=\"fr\">" + NEW_LINE + 
            "  <head>" + NEW_LINE + 
            "    <title>Ordinateurs mythiques</title>" + NEW_LINE + 
            "    <meta charset=\"utf-8\">" + NEW_LINE +
            "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">"+ NEW_LINE +
            "  </head>" + NEW_LINE + 
            "  <body>" + NEW_LINE +
            "    <header>" + NEW_LINE +
            "      <h1>Ordinateurs mythiques</h1>" + NEW_LINE +
            "    </header>" + NEW_LINE +
            "    <nav>" + NEW_LINE +
            "      <ul>" + NEW_LINE +
            "        <li><a href=\"index.html\">Accueil</a></li>" + NEW_LINE +
            "        <li><a href=\"produit1.html\">Produit 1</a></li>" + NEW_LINE +
            "        <li><a href=\"produit2.html\">Produit 2</a></li>" + NEW_LINE +
            "        <li><a href=\"produit3.html\">Produit 3</a></li>" + NEW_LINE +
            "        <li><a href=\"produit4.html\">Produit 4</a></li>" + NEW_LINE +
            "        <li><a href=\"produit5.html\">Produit 5</a></li>" + NEW_LINE +
            "      </ul>" + NEW_LINE +
            "    </nav>" + NEW_LINE +
            "    <main>" + NEW_LINE +
            "      <section>" + NEW_LINE +
            "        <h2>" + nom + " (" + entreprise + ")</h2>" + NEW_LINE +
            "        <h3>" + prix + " (Sortie en " + date + ")</h3>" + NEW_LINE +
            "        <p>" + NEW_LINE +
            description + NEW_LINE + 
            "        </p>" + NEW_LINE + 
            "      </section>" + NEW_LINE + 
            "    </main>" + NEW_LINE + 
            "  </body>" + NEW_LINE + 
            "</html>";
        }
        return(content);
    }

    void algorithm(){
        stringAsFile("output/index.html", genererSite("index"));
        stringAsFile("output/produit1.html", genererSite("produit1"));
        stringAsFile("output/produit2.html", genererSite("produit2"));
        stringAsFile("output/produit3.html", genererSite("produit3"));
        stringAsFile("output/produit4.html", genererSite("produit4"));
        stringAsFile("output/produit5.html", genererSite("produit5"));
    }
}
